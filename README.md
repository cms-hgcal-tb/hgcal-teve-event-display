# HGCAL TEVE event display
![alt text](hgcal_event.png)

## Generate geometry root file :
```
root -l hexagons.C
```

## Read the geometry root file (need to have hcgal_geom.root in current dir)
```
root -l readGeom.C
```

## Start event display (need to change file name in hgcal_eventDisplay.C, line 173)
```
root -l hgcal_eventDisplay.C
```

Use the top left buttons to navigate between events
